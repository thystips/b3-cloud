resource "opennebula_virtual_machine_group" "customers" {
  name        = "customers"
  group       = "oneadmin"
  permissions = "642"
  role {
    name              = "customers"
    policy            = "NONE"
  }
  tags = {
    environment = "customers"
  }
}

resource "opennebula_virtual_machine" "customers" {
  name        = "${var.instances[count.index].name}.${var.instances[count.index].domain}"
  template_id = 1
  permissions = "660"

  vmgroup {
    vmgroup_id = opennebula_virtual_machine_group.customers.id
    role       = opennebula_virtual_machine_group.customers.role[0].name
  }

  tags = {
    environment = "customers"
    customer = var.instances[count.index].customer_name
  }

  timeout = 5

  count = length(var.instances)
}

resource "ansible_host" "customers" {
    inventory_hostname = "${var.instances[count.index].name}.${var.instances[count.index].domain}"
    groups = ["master"]
    vars = {
        ansible_user = "root"
        domain_hosts = "${var.instances[count.index].domain}"
    }
    count = length(var.instances)
}