terraform {
  required_version = ">= 0.13"

  required_providers {
    ansible = {
      source  = "nbering/ansible"
      version = "1.0.4"
    }
    opennebula = {
      source = "OpenNebula/opennebula"
      version = "0.3.0"
    }
  }
}

provider "opennebula" {
  endpoint      = var.one_endpoint
  flow_endpoint = var.one_flow_endpoint
  username      = var.one_username
  password      = var.one_password
}