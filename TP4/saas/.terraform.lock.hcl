# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/nbering/ansible" {
  version     = "1.0.4"
  constraints = "1.0.4"
  hashes = [
    "h1:G9OI+CZMXBdfe0Oa+/B7zOVronatrovHxDlvpuy5VOE=",
    "zh:0601b46fc129828b7351662c46946a099e57c97a35eefdba111e97feaed00be1",
    "zh:0a6754c2b93146bbb50a6e7ac6d9b596da352bf43474ac65ebd0214ac703ce92",
    "zh:16cc61e49ab806a7e7b1e510d915c3563c89147e7f49d1cb857b4a98555d58e2",
    "zh:3ad5b27cc4ae71c0794b6c3a0abb5bbc74d750b34ab83bed0a232c722514dabe",
    "zh:4dca1f8fcf5a0749994346f1dc6c7f7284469b80d87370b5bae7e812234d1eba",
    "zh:587ef471ea30c9505fa39570c49c5ed2222d4569e5909982965a963209f5c67f",
    "zh:5cfaae8e4ef9754156008418fc74224461b7ad101fa86812af7315a7e79fd33f",
    "zh:720510a04d17c4593969dd31b44063a0ecca53d930e9616a7714c372b4973e13",
    "zh:84d57af64bfbd007440c6916c40a9bed6c5226b72549e8c408c80981bc9aa13e",
    "zh:e1fe7f6c9826f905333813332e027529d2dae30043c3903afcff05be302b940c",
    "zh:fe5e9e7a8550e97933e50d5471b4e13338754aff76524ecb1cb50bd6bce61145",
  ]
}

provider "registry.terraform.io/opennebula/opennebula" {
  version     = "0.3.0"
  constraints = "0.3.0"
  hashes = [
    "h1:soTMjoOFiuuMutBskniV9Ep4LX+u5Kh/iXjiBbUUvd4=",
    "zh:236aaa8ecd1e97f2c356281234142bbf7919db9265bad1584fc256bf4fcdd80d",
    "zh:2cb0987f477fcb5543a81c747f56f6b2ed48e10ca81975f5b0bf6dd5862587b9",
    "zh:40b0a342b3e46f3173eb47cbcfe65cac0816327b383cba54a6631406adf920d6",
    "zh:46e952f79180fda444ee0a781ab13b98cfe56e67aea1bc5fdfb5be0a940fa03f",
    "zh:8b1c1c8bd2424382c237609b1b3587075f872e1301043734aa9d08a577446ffa",
    "zh:8c30d3135b768ae95cf2019114506b137529ed6354bd664496f126abc72c4c14",
    "zh:94bc3a8acf1286cde7cb247d8dd93c028d2bc385379ffa654b38432dd924c390",
    "zh:aa40d6719796c30bea613f152c49072a4eebfe09295e7c69e6bbf37567d93f28",
    "zh:df9a6acef4d4034095859d16481248640f73a348cba77cd987c37fdbd6c5b7c1",
    "zh:e8ee96066623e2738f6b886e9c98b27be442bbe151dfe19beaeb731bfd569086",
    "zh:ed0e8438409eda95a44bac40ce472fd705fccabeed95cf30ea300c3506936c75",
  ]
}
