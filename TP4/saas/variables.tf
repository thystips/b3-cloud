variable "one_endpoint" {
  sensitive = true
  type = string
}
variable "one_username" {
  sensitive = true
  type = string
}
variable "one_password" {
  sensitive = true
  type = string
}
variable "one_flow_endpoint" {
  sensitive = true
  type = string
}

variable "instances" {
    type = list(object({
        name = string
        domain = string
        customer_name = string
    }))  
}