#/bin/env bash

yum install -y epel-release
yum install -y dnf python python3

yum update -y
systemctl enable --now firewalld

firewall-cmd --add-port=9869/tcp --add-port=29876/tcp --add-port=2633/tcp --add-port=2474/tcp --add-port=5030/tcp --permanent
firewall-cmd --reload

cat << "EOF" > /etc/hosts
172.30.100.11 frontend.tp3
172.30.100.12 kvm1.tp3
172.30.100.13 kvm2.tp3
EOF

cat << "EOT" > /etc/yum.repos.d/opennebula.repo
[opennebula]
name=OpenNebula Community Edition
baseurl=https://downloads.opennebula.io/repo/5.12/CentOS/7/$basearch
enabled=1
gpgkey=https://downloads.opennebula.io/repo/repo.key
gpgcheck=1
repo_gpgcheck=1
EOT

yum makecache fast -y

modprobe bridge

yum install bridge-utils -y

cat << EOF > /etc/sysconfig/network-scripts/ifcfg-virbr0
DEVICE="virbr0"
BOOTPROTO="static"
NETMASK="255.255.255.0"
ONBOOT="yes"
TYPE="Bridge"
NM_CONTROLLED="no"
EOF

echo "IPADDR=${BRIDGE_IP}" >> /etc/sysconfig/network-scripts/ifcfg-virbr0

cat << EOF > /etc/sysconfig/network-scripts/ifcfg-eth2
NM_CONTROLLED=no
BOOTPROTO=none
ONBOOT=yes
DEVICE=eth2
BRIDGE=virbr0
EOF

systemctl restart network