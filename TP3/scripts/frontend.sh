#/bin/env bash

yum install opennebula-server opennebula-sunstone opennebula-ruby opennebula-gate opennebula-flow -y

sudo -u oneadmin mkdir /home/oneadmin/.one
sudo -u oneadmin echo "oneadmin:password" > /home/oneadmin/.one/one_auth

systemctl enable --now opennebula
systemctl enable --now opennebula-sunstone

cp /var/lib/one/.ssh/authorized_keys /vagrant/.ssh/
cp /var/lib/one/.ssh/id_rsa.pub /vagrant/.ssh/
cp /var/lib/one/.ssh/id_rsa /vagrant/.ssh/