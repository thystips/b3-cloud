#/bin/env bash

echo "Cette étape doit être effectuée sur le dernier hôte de virtu à être lancé"

ssh-keyscan frontend.tp3 kvm1.tp3 kvm2.tp3 | sudo -u oneadmin tee -a /var/lib/one/.ssh/known_hosts

scp -o UserKnownHostsFile=/var/lib/one/.ssh/known_hosts -i /var/lib/one/.ssh/id_rsa -p /var/lib/one/.ssh/known_hosts oneadmin@frontend.tp3:/var/lib/one/.ssh/ 
scp -o UserKnownHostsFile=/var/lib/one/.ssh/known_hosts -i /var/lib/one/.ssh/id_rsa -p /var/lib/one/.ssh/known_hosts oneadmin@kvm1.tp3:/var/lib/one/.ssh/ 