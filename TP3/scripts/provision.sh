#/bin/env bash

cat << "EOF" > /etc/hosts
172.30.100.11 frontend.tp3
172.30.100.12 kvm1.tp3
172.30.100.13 kvm2.tp3
172.30.100.14 file0.tp3
EOF

yum install -y python python-pip python3 python3-pip
