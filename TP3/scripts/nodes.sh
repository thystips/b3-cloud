#/bin/env bash

yum install centos-release-qemu-ev -y

yum install qemu-kvm-ev -y

yum install opennebula-node-kvm -y

systemctl restart libvirtd
systemctl enable libvirtd

cp /vagrant/.ssh/id_rsa /var/lib/one/.ssh/
cp /vagrant/.ssh/id_rsa.pub /var/lib/one/.ssh/
cp /vagrant/.ssh/authorized_keys /var/lib/one/.ssh/

chown oneadmin:oneadmin -R /var/lib/one/.ssh/