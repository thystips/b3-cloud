#/bin/env bash

cat > /etc/hosts <<EOF>
192.168.56.11 registry.b3
192.168.56.11 traefik.b3
192.168.56.11 python.b3
192.168.56.11 minio.b3
192.168.56.11 monitoring.b3
192.168.56.11 grafana.monitoring.b3
192.168.56.11 alertmanager.monitoring.b3
192.168.56.11 unsee.monitoring.b3
192.168.56.11 prometheus.monitoring.b3
EOF