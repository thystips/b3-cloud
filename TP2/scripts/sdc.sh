#/bin/env bash

# sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sdc
#   o
#   n
#   p
#   1
   
   
#   p
#   w
#   q
# EOF

yum -y install lvm2

systemctl enable --now lvm2-lvmetad
systemctl enable --now lvm2-monitor

pvcreate -v /dev/sdc
vgcreate -s 256k -v vg_minio_2 /dev/sdc
lvcreate -l +100%FREE -v vg_minio_2 -n lv_minio_2

mkfs.ext4 /dev/vg_minio_2/lv_minio_2

mkdir /minio-2

mount /dev/vg_minio_2/lv_minio_2 /minio-2

#echo 'type=83' | sfdisk /dev/sdc
#yes | mkfs.ext4 /dev/sdc1