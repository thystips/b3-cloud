#!/bin/env bash

cp /vagrant/service/restic.service /etc/systemd/system/
cp /vagrant/service/restic.timer /etc/systemd/system/

systemctl enable restic.timer
systemctl start restic.timer
