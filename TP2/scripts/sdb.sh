#/bin/env bash

# sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sdb
#   o
#   n
#   p
#   1
   
   
#   p
#   w
#   q
# EOF

yum -y install lvm2

systemctl enable --now lvm2-lvmetad
systemctl enable --now lvm2-monitor

pvcreate -v /dev/sdb
vgcreate -s 256k -v vg_minio /dev/sdb
lvcreate -l +100%FREE -v vg_minio -n lv_minio

mkfs.ext4 /dev/vg_minio/lv_minio

mkdir /minio

mount /dev/vg_minio/lv_minio /minio

#echo 'type=83' | sfdisk /dev/sdb
#yes | mkfs.ext4 /dev/sdb1