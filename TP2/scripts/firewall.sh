#/bin/env bash

# Docker swarm
firewall-cmd --add-port=2377/tcp --add-port=7946/tcp --add-port=7946/udp --add-port=4789/udp --permanent

# Registry
firewall-cmd --add-port=5000/tcp --permanent

#Traefik
firewall-cmd --add-service=http --permanent
firewall-cmd --add-service=https --permanent

firewall-cmd --reload