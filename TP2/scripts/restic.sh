#!/bin/env bash

yum install yum-plugin-copr -y
yum copr enable copart/restic -y
yum install restic -y