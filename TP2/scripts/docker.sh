#/bin/env bash

mkdir /etc/docker
mv /tmp/daemon.json /etc/docker/daemon.json

systemctl enable --now docker
usermod -aG docker vagrant