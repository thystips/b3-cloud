#!/bin/env bash

cd /vagrant/swarmprom

export ADMIN_USER=admin
export ADMIN_PASSWORD=password
export HASHED_PASSWORD=$(openssl passwd -apr1 $ADMIN_PASSWORD)
export DOMAIN=monitoring.b3

docker stack deploy -c docker-compose.traefik.yml swarmprom